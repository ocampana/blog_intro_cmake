#include "hello.h"

#include "config.h"

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include <stdio.h>

#define BUFSIZE 64

void
say_hello (void)
{
    char *c;
    char name[BUFSIZE];

    fprintf (stdout, "Inserisci il tuo nome: ");

    fgets (name, BUFSIZE, stdin);

    /*
     * dobbiamo rimuovere il newline finale in eccesso.
     * se vogliamo essere portabili dobbiaom gestire i casi
     * unix \n
     * windows \r\n
     * Mac OS <= 9 \r
     */

#if defined (HAVE_INDEX)
    c = index (name, '\n');
    if (c != NULL)
        *c = 0;

    c = index (name, '\r');
    if (c != NULL)
        *c = 0;
#elif defined (HAVE_STRCHR)
    c = strchr (name, '\n');
    if (c != NULL)
        *c = 0;

    c = strchr (name, '\r');
    if (c != NULL)
        *c = 0;
#else
#error "no function available"
#endif

    fprintf (stdout, "Ciao %s\n", name);
}
